// [SECTIONS] Dependencies and Modules
	const mongoose = require('mongoose');

// [SECTIONS] Schema (Blueprint) 
	const cartSchema = new mongoose.Schema({
		userId: {
			type: String,
			required: [true, 'error']
		},
		productCart: [
			{
				productId:{
					type: String,
					required: [true, 'error']
				},
				name:{
					type: String,
					required: [true, 'error']
				},
				price:{
					type: Number,
					required: [true, 'error']
				},
				quantity:{
					type: Number,
					required: [true, 'error']
				},	
				subTotal: {
					type: Number,
					required: [true, 'error']
				}
			}
		]
	});

// [SECTIONS] Model
	module.exports = mongoose.model('Cart',cartSchema);