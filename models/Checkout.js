// [SECTIONS] Dependencies and Modules
	const mongoose = require('mongoose');

// [SECTIONS] Schema (Blueprint) 
	const checkoutSchema = new mongoose.Schema({
		userId: {
			type: String,
			required: [true, 'error']
		},
		firstName: {
			type: String,
			required: [true, 'error']
		},
		lastName: {
			type: String,
			required: [true, 'error']
		},
		products: [
			{
				productId:{
					type: String,
				},
				name:{
					type: String,
				},
				price:{
					type: Number,
				},
				quantity:{
					type: Number,
				},
				subTotal: {
					type: Number,
				}
			}
		],
		totalAmount: {
			type: Number,
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	});

// [SECTIONS] Model
	module.exports = mongoose.model('Checkout',checkoutSchema);