// [SECTION] Modules and Dependencies
	const mongoose = require('mongoose');

// [SECTION] Schema (Blueprint)
	const userSchema = new mongoose.Schema({
		firstName:{
			type: String,
			required: [true, 'Please fill up your First Name']
		},
		lastName:{
			type: String,
			required: [true, 'Please fill up your Last Name']
		},
		email:{
			type: String,
			required: [true, 'Please fill up your Email Address']
		},
		contactNo:{
			type: String,
			required: [true, 'Please fill up your Phone Number']
		},
		password:{
			type: String,
			required: [true, 'Your password is empty']
		},
		isAdmin:{
			type: Boolean,
			default: false
		},
	});

// [SECTION] Model
	module.exports = mongoose.model('User', userSchema);