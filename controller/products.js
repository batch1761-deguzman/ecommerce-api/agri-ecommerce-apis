// [SECTION] Dependencies and modules
	const Product = require ('../models/Product')

// [SECTION] Functionalities [CREATE]
	module.exports.addProduct = (productInfo, productImage) => {
		let newProduct = new Product({
			productName: productInfo.name,
			description: productInfo.description,
			price: productInfo.price,
			image: productImage
		});

		return newProduct.save().then((product, error) => {
			if (product) {
				return product;
			} else {
				return false;
			};
		}).catch(error => error.message);
	};

// [SECTION] Functionalities [RETRIEVE]
	// [SUBSECTION] Retrieve active Products
		module.exports.getAllActiveProducts = () => {
			return Product.find({isActive: true}).then(outcome => {
				return outcome;
			}).catch(error => error);
		};

	// [SUBSECTION] Retrieve active featured Products
			module.exports.getAllActiveFeaturedProducts = () => {
				return Product.find({isActive: true, isFeature: true}).then(outcome => {
					return outcome;
				}).catch(error => error);
			};

	// [SUBSECTION] Retrieve Search and Active Products
		module.exports.getAllActiveSearchProducts = (productName) => {
			return Product.find({productName: new RegExp(productName,'i'), isActive: true}).then(outcome => {
				console.log(outcome)
				if (outcome.length === 0 ) {
					return false;
				} else {
					return outcome;
				}
			}).catch(error => error);
		};

	// [SUBSECTIONS] Retrieve ALL Products (admin)
		module.exports.getAllProducts = () => {
			return Product.find({}).then(outcome =>{
				return outcome;
			});
		};

	// [SUBSECTIONS] Retrieving Specific Product
		module.exports.getProduct = (paramId) => {
			return Product.findById(paramId).then(outcome => {
				if (outcome) {
					return outcome;
				} else {
					return { message: "file not in database"}
				}
			}).catch(error => error.message);
		};

// [SECTION] Functionalities [UPDATE]
	// [SUBSECTION] Updating Products (Admin)
		module.exports.updateProduct = (productId, productData) => {
			let updatedProductInfo = {
				productName: productData.name,
				description: productData.description,
				price: productData.price
			};

			return Product.findByIdAndUpdate(productId, updatedProductInfo).then((product,error) => {
				if (product) {
					return true;
				} else {
					return false;
				};
			});
		};

	// [SUBSECTION] Archiving Products (Admin)
		module.exports.archiveProduct = (productId) => {
			let productStatus = {
				isActive: false
			};
			return Product.findByIdAndUpdate(productId, productStatus).then((product, error) => {
				if (product) {
					return { Update: "Success"};
				} else {
					return false;
				};
			}).catch(error => error);
		};
		
		// [SUBSECTION] Un-Archiving Products (Admin)
			module.exports.unarchiveProduct = (productId) => {
				let productStatus = {
					isActive: true
				};
				return Product.findByIdAndUpdate(productId, productStatus).then((product, error) => {
					if (product) {
						return { Update: "Success"};
					} else {
						return false;
					};
				}).catch(error => error);
			};

		// [SUBSECTION] Featuring Products (Admin)
			module.exports.featuringProduct = (productId) => {
				let productStatus = {
					isFeature: true
				};
				return Product.findByIdAndUpdate(productId, productStatus).then((product, error) => {
					if (product) {
						return { Update: "Success"};
					} else {
						return false;
					};
				}).catch(error => error);
			};
		
		// [SUBSECTION] Un-Featuring Products (Admin)
			module.exports.unfeatureProduct = (productId) => {
				let productStatus = {
					isFeature: false
				};
				return Product.findByIdAndUpdate(productId, productStatus).then((product, error) => {
					if (product) {
						return { Update: "Success"};
					} else {
						return false;
					};
				}).catch(error => error);
			};
			
// [SECTION] Functionalities [DELETE]
	module.exports.deleteProduct = (productId) => {
		return Product.findByIdAndRemove(productId).then((product,error) => {
			if (product) {
				return { Delete: "Complete"};
			} else {
				return false;
			};
		}).catch(error => error);
	};