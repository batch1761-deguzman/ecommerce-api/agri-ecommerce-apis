// [SECTION] Dependencies and modules
	const User = require('../models/User');
	const Product = require('../models/Product');
	const Order = require('../models/Order');  
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth');

// [SECTION] Environment Variables Setup
	dotenv.config();
	const salt = parseInt(process.env.SALT);

// [SECTION] Functionalities [CREATE]
	// [SUBSECTION] Create new user
		module.exports.register = (userData) => {
			let fName = userData.firstName;
			let lName = userData.lastName;
			let email = userData.email;
			let ctNum = userData.contactNo;
			let pWord = userData.password;

			let newUser = new User({
				firstName: fName,
				lastName: lName,
				email: email,
				contactNo: ctNum,
				password: bcrypt.hashSync(pWord, salt)
			});

			return newUser.save().then((user, err) => {
				if (user) {
					return user;
				} else {
					return 'Failed to Register account';
				};
			}).catch(error => error.message);
		};

// [SECTION] Functionalities [RETRIEVE]
	// [SUBSECTION] Retrieve Authenticated(logged in) User Details
		module.exports.getUserProfile = (userData)  => {
			return User.findById(userData).then(outcome => {
				outcome.password = '';
				return outcome;
			});
		};

	// [SUBSECTION] View ALL Registered Users (admin)
		module.exports.retrieveAllUsers = () => {
			return User.find({}).then(outcome => {
				return outcome;
			});
		};

	// [SUBSECTION] Promote User to Admin (admin)
		module.exports.promoteAdmin = (profileId) => {
			let adminField = {
				isAdmin: true
			};

			return User.findByIdAndUpdate(profileId, adminField).then((profile, error) => {
				if (profile) {
					return { message:"successfully promoted"};
				} else {
					return false;
				};
			}).catch(error => error.message);
		}
		
// [SECTION] Functionalities [UPDATE]
// [SECTION] Functionalities [DELETE]

// [SECTION] User Authentication
	module.exports.userLogin = (loginData) => {
		return User.findOne({email: loginData.email}).then(outcome => {
			if (outcome) {
				const passwordCheck = bcrypt.compareSync(loginData.password, outcome.password);

				if (passwordCheck) {
					return {accessToken: auth.createAccessToken(outcome.toObject())};
				} else {
					return false;
				};
			} else {
				return false;
			};
		});
	};
