// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controller/orders');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Authentication Destructuring 
	const {verify, verifyAdmin} = auth;
	
// [SECTION] Routes - POST 
	// [SUBSECTION] User Checkout (Order Placement)
		route.post('/purchase', auth.verify, controller.purchase);

// [SECTION] Routes - GET
	// [SUBSECTION] Retrieve Authenticated User Orders (User)
		route.get('/viewpurchase', auth.verify, (req, res) => {
			controller.getOrders(req.user.id).then(outcome => res.send(outcome));
		})

	// [SUBSECTION] Retrieve Records of ALL Orders (Admin)
		route.get('/allorder', verify, verifyAdmin, (req, res) => {
			controller.getAllOrders().then(outcome => res.send(outcome));
		})

// [SECTION] Routes - PUT
// [SECTION] Routes - DEL

// [SECTION] Expose Routing System
	module.exports = route;