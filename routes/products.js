// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controller/products');
	const auth = require('../auth');
	const Product = require ('../models/Product');

	const multer = require('multer');
	const storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, './uploads/');
		},
		filename: function(req, file, cb) {
			cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
		}
	});

	const fileFilter = (req, file, cb) => {
		if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpeg') {

		} else {
			cb(null, true);
		}
			cb(null, false);
	}

	const upload = multer({ 
		storage: storage, 
		limits: {
			fileSize: 1024 * 1024 * 5
		},
	});
	
// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Authentication Destructuring 
	const {verify, verifyAdmin} = auth;

// [SECTION] Routes - POST
	
	route.post('/create', upload.single('image'), verify, verifyAdmin, (req, res) =>{
		let orderCheck = req.body.name;
		return Product.findOne({productName: orderCheck}).then(result => {
			if (result) {
				res.send({ message: "Product already exists in database"});
			} else {
				controller.addProduct(req.body, req.file.path).then(outcome => res.send(outcome));
			};
		})
		 
	});

// [SECTION] Routes - GET
	// [SUBSECTION] - Retrieve ACTIVE products
		route.get('/active', (req, res) => {
			controller.getAllActiveProducts().then(outcome => res.send(outcome));
		});

	// [SUBSECTION] - Retrieve Active Featured products
			route.get('/feature', (req, res) => {
				controller.getAllActiveFeaturedProducts().then(outcome => res.send(outcome));
			});

	// [SUBSECTION] - Retrieve Seached ACTIVE products
		route.get('/active/:productName', (req, res) => {
			let productName = req.params.productName;
			controller.getAllActiveSearchProducts(productName).then(outcome => res.send(outcome));
		});

	// [SUBSECTION] - Retrieve ALL products (admin)
		route.get('/all', (req, res) => {
			controller.getAllProducts().then(outcome => res.send(outcome));
		});

	// [SUBSECTION] - retriev SPECIFIC product 
		route.get('/:productId', (req, res) => {
			let productId = req.params.productId;
			controller.getProduct(productId).then(outcome => res.send(outcome));
		});

// [SECTION] Routes - PUT
	// [SUBSECTION] Updating product information
		route.put('/:productId', verify, verifyAdmin, (req, res) => {
			let productId = req.params.productId;
			let productDetails = req.body;
			controller.updateProduct(productId, productDetails).then(outcome => res.send(outcome));
		});

	// [SUBSECTION] Archiving product (admin)
		route.put('/:productId/archive', verify, verifyAdmin, (req, res) => {
			let productId = req.params.productId;
			controller.archiveProduct(productId).then(outcome => res.send(outcome));
		});

	// [SUBSECTION] Un-Archiving product (admin)
		route.put('/:productId/unarchive', verify, verifyAdmin, (req, res) => {
			let productId = req.params.productId;
			controller.unarchiveProduct(productId).then(outcome => res.send(outcome));
		});

	// [SUBSECTION] Featuring product (admin)
	route.put('/:productId/featuring', verify, verifyAdmin, (req, res) => {
		let productId = req.params.productId;
		controller.featuringProduct(productId).then(outcome => res.send(outcome));
	});

	// [SUBSECTION] Remove Feature product (admin)
		route.put('/:productId/removefeature', verify, verifyAdmin, (req, res) => {
			let productId = req.params.productId;
			controller.unfeatureProduct(productId).then(outcome => res.send(outcome));
		});

// [SECTION] Routes - DEL
	route.delete('/:productId/delete', verify, verifyAdmin, (req, res) => {
		let productId = req.params.productId;
		controller.deleteProduct(productId).then(outcome => res.send(outcome));
	});
	
// [SECTION] Expose Routing System
	module.exports = route;