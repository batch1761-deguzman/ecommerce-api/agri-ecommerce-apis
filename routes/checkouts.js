// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controller/checkouts');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Authentication Destructuring 
	const {verify, verifyAdmin} = auth;
	
// [SECTION] Routes - POST 
	// [SUBSECTION] User Checkout (Order Placement)
		route.post('/final', auth.verify, controller.purchase);

// [SECTION] Routes - GET
	// [SUBSECTION] Retrieve Authenticated User Checkout History (User)
		route.get('/view', auth.verify, (req, res) => {
			controller.getOrders(req.user.id).then(outcome => res.send(outcome));
		})

	// [SUBSECTION] Retrieve Authenticated User Checkout History Products (User)
	route.get('/:_id/viewproducts/', auth.verify, (req, res) => {
		controller.getOrderProducts(req.params._id).then(outcome => res.send(outcome));
	})

	// [SUBSECTION] Retrieve Records of ALL Checkouts (Admin)
		route.get('/allcheckout', verify, verifyAdmin, (req, res) => {
			controller.getAllOrders().then(outcome => res.send(outcome));
		})

// [SECTION] Routes - PUT
// [SECTION] Routes - DEL

// [SECTION] Expose Routing System
	module.exports = route;