// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controller/carts');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Authentication Destructuring 
	const {verify, verifyAdmin} = auth;
	
// [SECTION] Routes - POST 
	// [SUBSECTION] Adding Products to Cart by Authenticated User
		route.post('/addcart', auth.verify, controller.purchase);

// [SECTION] Routes - GET
	// [SUBSECTION] Retrieve Authenticated User Orders from Cart(User)
		route.get('/view', auth.verify, (req, res) => {
			controller.getOrders(req.user.id).then(outcome => res.send(outcome));
		})

// [SECTION] Routes - PUT
	// [SUBSECTION] Updating product information
		/*route.put('/:cartId', auth.verify,  (req, res) => {
			let cartId = req.params.cartId;
			let cartDetails = req.body;
			controller.updateCart(req.user.id, cartId, cartDetails).then(outcome => res.send(outcome));
		});*/
		route.put('/:cartId', auth.verify, controller.updateCart);


// [SECTION] Routes - DEL
	route.delete('/:cartId/delete', auth.verify, (req, res) => {
			let cartId = req.params.cartId;
			controller.deleteCart(req.user.id, cartId).then(outcome => res.send(outcome));
		});

// [SECTION] Expose Routing System
	module.exports = route;