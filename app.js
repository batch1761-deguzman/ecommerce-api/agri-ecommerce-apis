// [SECTION] Dependencies and Modules
	const express = require('express');	
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const userRoutes = require('./routes/users');
	const productRoutes = require('./routes/products');
	const orderRoutes = require('./routes/orders')
	const cartRoutes = require('./routes/carts')
	const checkoutRoutes = require('./routes/checkouts')
	const cors = require('cors');

	

// [SECTION] Environment
	dotenv.config();
	let dbConnect = process.env.DBCREDENTIALS;
	const port = process.env.PORT;

// [SECTION] Server
	const agriApp = express();
	agriApp.use(express.json());
	agriApp.use(cors());

// [SECTION] Database Conenction
	mongoose.connect(dbConnect);
	const conStatus = mongoose.connection;
	conStatus.once('open', () => console.log('Database Connection Established'));

// [SECTION] Backend Routes
	agriApp.use('/uploads',express.static('uploads'));
	agriApp.use('/users', userRoutes);
	agriApp.use('/products', productRoutes);
	agriApp.use('/orders', orderRoutes);
	agriApp.use('/carts', cartRoutes);
	agriApp.use('/checkouts', checkoutRoutes);

// [SECTION] Gateway Response
	agriApp.get('/', (req, res) => {
		res.send('Landing Page')
	});
	agriApp.listen(port, () => {
		console.log(`Agri Ecommerce API running on ${port}`);
	});